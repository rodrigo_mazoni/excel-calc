package com.calc.excel.demo.controller;


import com.calc.excel.demo.component.ExcelReaderComponent;
import com.calc.excel.demo.model.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;

@Controller
@RequestMapping("/api/movimentacao")
public class MovimentacaoDiariaController {

    @Autowired
    private ExcelReaderComponent excelReaderComponent;

    @GetMapping
    public ResponseEntity<List<?>> buscarTodos() throws IOException {
        List<Item> lista = excelReaderComponent.percorreDocumentos();
        return new ResponseEntity<>(lista.stream().map(a -> a.getMovimentacaoDiariaList())
                                            .collect(Collectors.toList()), HttpStatus.OK);
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<?> buscaPorId(@PathVariable(name = "id") String id) throws IOException {
        List<Item> lista = excelReaderComponent.percorreDocumentos();
        return new ResponseEntity<>(lista.stream().filter(f -> f.getItem().equals(id))
                                            .map(a -> a.getMovimentacaoDiariaList())
                                            .collect(Collectors.toList()), HttpStatus.OK);
    }

}

