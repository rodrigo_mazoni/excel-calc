package com.calc.excel.demo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class MovimentacaoDiaria {

    private String item;
    private LocalDate data_lancamento;
    private double lancEntQtd;
    private double lancEntValor;
    private double lancSaiQtd;
    private double lancSaiValor;
    private double saldoInicialQtd;
    private double saldoInicialValor;
    private double saldoFinalQtd;
    private double saldoFinalValor;

}
