package com.calc.excel.demo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Data
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class Item {

    private String item;
    private LocalDate data_inicio;
    private Double qtd_inicio;
    private Double valor_inicio;
    private LocalDate data_final;
    private Double qtd_final;
    private Double valor_final;

    private List<MovimentacaoDiaria> movimentacaoDiariaList = new ArrayList<>();


}
