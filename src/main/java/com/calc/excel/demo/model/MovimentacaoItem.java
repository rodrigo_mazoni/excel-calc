package com.calc.excel.demo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class MovimentacaoItem {

    private String item;
    private String tipo_movimento;
    private LocalDate data_lancamento;
    private Double quantidade;
    private Double valor;

}
