package com.calc.excel.demo.component;

import com.calc.excel.demo.model.Item;
import com.calc.excel.demo.model.MovimentacaoDiaria;
import com.calc.excel.demo.model.MovimentacaoItem;
import com.sun.org.apache.xpath.internal.operations.Bool;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.Objects.nonNull;

@Component
public class ExcelReaderComponent {

    private static String[] columns = {"item", "data_lancamento", "lancEntQtd", "lancEntValor", "lancSaiQtd", "lancSaiValor", "saldoInicialQtd"
                            ,  "saldoInicialValor", "saldoFinalQtd", "saldoFinalValor"};

    public ExcelReaderComponent() throws IOException {
        List<Item> itens = percorreDocumentos();

        Workbook workbook = new XSSFWorkbook();
        CreationHelper createHelper = workbook.getCreationHelper();

        itens.stream().forEach(item -> {
            Sheet sheet = workbook.createSheet(item.getItem());

            Font headerFont = workbook.createFont();
            headerFont.setBold(true);
            headerFont.setFontHeightInPoints((short) 14);
            headerFont.setColor(IndexedColors.RED.getIndex());

            CellStyle headerCellStyle = workbook.createCellStyle();
            headerCellStyle.setFont(headerFont);

            Row headerRow = sheet.createRow(0);

            for(int i = 0; i < columns.length; i++) {
                Cell cell = headerRow.createCell(i);
                cell.setCellValue(columns[i]);
                cell.setCellStyle(headerCellStyle);
            }

            CellStyle dateCellStyle = workbook.createCellStyle();
            dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd-MM-yyyy"));

            int rowNum = 1;
            for(MovimentacaoDiaria mov: item.getMovimentacaoDiariaList()) {
                Row row = sheet.createRow(rowNum++);
                row.createCell(0).setCellValue(item.getItem());
                row.createCell(1).setCellValue(toString(mov.getData_lancamento()));
                row.createCell(2).setCellValue(mov.getLancEntQtd());
                row.createCell(3).setCellValue(mov.getLancEntValor());
                row.createCell(4).setCellValue(mov.getLancSaiQtd());
                row.createCell(5).setCellValue(mov.getLancSaiValor());
                row.createCell(6).setCellValue(mov.getSaldoInicialQtd());
                row.createCell(7).setCellValue(mov.getSaldoInicialValor());
                row.createCell(8).setCellValue(mov.getSaldoFinalQtd());
                row.createCell(9).setCellValue(mov.getSaldoFinalValor());
            }

            for(int i = 0; i < columns.length; i++) {
                sheet.autoSizeColumn(i);
            }
        });


        FileOutputStream fileOut = new FileOutputStream("resultado.xlsx");
        workbook.write(fileOut);
        fileOut.close();
        workbook.close();
    }

    public List<Item> percorreDocumentos() throws IOException {
        List<Item> listaItem = new ArrayList<>();
        List<MovimentacaoItem> movimentacaoItems = new ArrayList<>();

        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        percorreXlsSaldoItem(listaItem, classloader);
        percorreXlsMovSaldoItem(movimentacaoItems, classloader);

        return listaItem.stream().map(item -> {
            Map<LocalDate, List<MovimentacaoItem>> mapaMovimentacaoAnual = movimentacaoItems.stream()
                    .filter(f -> f.getItem().equals(item.getItem()))
                    .collect(Collectors.groupingBy(a -> a.getData_lancamento())).entrySet().stream()
                    .sorted(Map.Entry.comparingByKey())
                    .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (oldValue, newValue) -> oldValue, LinkedHashMap::new));

            MovimentacaoDiaria movimentacaoAnterior = null;
            Boolean isPrimeiraIteracao = true;

            for (Map.Entry<LocalDate, List<MovimentacaoItem>> localDateListEntry : mapaMovimentacaoAnual.entrySet()) {

                LocalDate dia = localDateListEntry.getKey();
                MovimentacaoDiaria movDiaria = new MovimentacaoDiaria();
                movDiaria.setData_lancamento(dia);
                movDiaria.setItem(item.getItem());

                List<MovimentacaoItem> movimentacao = localDateListEntry.getValue().stream().sorted(Comparator.comparing(MovimentacaoItem::getData_lancamento)).collect(Collectors.toList());

                if (isPrimeiraIteracao) {
                    movDiaria.setSaldoInicialQtd(item.getQtd_inicio());
                    movDiaria.setSaldoInicialValor(item.getValor_inicio());
                } else if (nonNull(movimentacaoAnterior)) {
                    movDiaria.setSaldoInicialQtd(movimentacaoAnterior.getSaldoFinalQtd());
                    movDiaria.setSaldoInicialValor(movimentacaoAnterior.getSaldoFinalValor());
                }

                movDiaria.setLancEntQtd(movimentacao.stream().filter(el -> el.getTipo_movimento().equals("Ent"))
                        .mapToDouble(element -> element.getQuantidade()).sum());
                movDiaria.setLancEntValor(movimentacao.stream().filter(el -> el.getTipo_movimento().equals("Ent"))
                        .mapToDouble(element -> element.getValor()).sum());

                movDiaria.setLancSaiQtd(movimentacao.stream().filter(el -> !el.getTipo_movimento().equals("Ent"))
                        .mapToDouble(element -> element.getQuantidade()).sum());
                movDiaria.setLancSaiValor(movimentacao.stream().filter(el -> !el.getTipo_movimento().equals("Ent"))
                        .mapToDouble(element -> element.getValor()).sum());

                Double calculoMovimentacaoDiaria = movimentacao.stream().mapToDouble(element -> getValueByTipoMov(element)).reduce(0L, (soma, valor) -> soma + valor);
                Double quantidadesDiarias = movimentacao.stream().mapToDouble(element -> getQtdByMov(element)).reduce(0L, (soma, valor) -> soma + valor);

                movDiaria.setSaldoFinalQtd(movDiaria.getSaldoInicialQtd() + quantidadesDiarias);
                movDiaria.setSaldoFinalValor(movDiaria.getSaldoInicialValor() + calculoMovimentacaoDiaria);

                item.getMovimentacaoDiariaList().add(movDiaria);
                movimentacaoAnterior = movDiaria;
                isPrimeiraIteracao = false;
            }

            return item;
        }).collect(Collectors.toList());
    }

    private double getQtdByMov(MovimentacaoItem element) {
        return element.getTipo_movimento().equals("Ent") ? element.getQuantidade() : (element.getQuantidade() * -1);
    }

    private double getValueByTipoMov(MovimentacaoItem element) {
        return element.getTipo_movimento().equals("Ent") ? element.getValor() : (element.getValor() * -1);
    }

    private void percorreXlsSaldoItem(List<Item> listaItem, ClassLoader classloader) throws IOException {
        URL url = classloader.getResource("SaldoITEM.xlsx");
        Iterator<Row> iterator = mostraEstruturaIteratorIgnorandoColunaTitulo(url);

        while (iterator.hasNext()) {
            Row currentRow = iterator.next();

            Item item = new Item(
                    currentRow.getCell(0).getStringCellValue(),
                    toLocalDate(currentRow.getCell(1).getDateCellValue()),
                    currentRow.getCell(2).getNumericCellValue(),
                    currentRow.getCell(3).getNumericCellValue(),
                    toLocalDate(currentRow.getCell(4).getDateCellValue()),
                    currentRow.getCell(5).getNumericCellValue(),
                    currentRow.getCell(6).getNumericCellValue(),
                    new ArrayList<>()
            );
            listaItem.add(item);
        }
    }

    private void percorreXlsMovSaldoItem(List<MovimentacaoItem> listaItem, ClassLoader classloader) throws IOException {
        URL url = classloader.getResource("MovtoITEM.xlsx");
        Iterator<Row> iterator = mostraEstruturaIteratorIgnorandoColunaTitulo(url);

        while (iterator.hasNext()) {
            Row currentRow = iterator.next();

            MovimentacaoItem item = new MovimentacaoItem(
                    currentRow.getCell(0).getStringCellValue(),
                    currentRow.getCell(1).getStringCellValue(),
                    toLocalDate(currentRow.getCell(2).getDateCellValue()),
                    currentRow.getCell(3).getNumericCellValue(),
                    currentRow.getCell(4).getNumericCellValue()
            );
            listaItem.add(item);
        }
    }

    private Iterator<Row> mostraEstruturaIteratorIgnorandoColunaTitulo(URL url) throws IOException {
        FileInputStream excelFile = new FileInputStream(new File(url.getFile()));
        Workbook workbook = new XSSFWorkbook(excelFile);
        Sheet datatypeSheet = workbook.getSheetAt(0);
        Iterator<Row> iterator = datatypeSheet.iterator();
        iterator.next();
        return iterator;
    }

    private LocalDate toLocalDate(Date date) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        return LocalDate.parse(new SimpleDateFormat("dd/MM/yyyy").format(date), formatter);
    }

    private String toString(LocalDate date) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        return formatter.format(date);
    }
}
